const cards = [
    ['Spade','A'],
    ['Diamond','J'],
    ['Club','3'],
    ['Heart','6'],
    ['Spade','K'],
    ['Club','2'],
    ['Heart','Q'],
    ['Spade','6'],
    ['Heart','J'],
    ['Spade','10'],
    ['Club','4'],
    ['Diamond','Q'],
    ['Diamond','3'],
    ['Heart','4'],
    ['Club','7']
];

function compareCard(cardA,cardB){
    const ranks = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"];
    const suits = ["Diamond","Club","Heart","Spade"];
    const [suitA,rankA] = cardA;
    const [suitB, rankB] = cardB;
    const ranksDiff = ranks.indexOf(rankA) - ranks.indexOf(rankB);
    if(ranksDiff !== 0){
      return ranksDiff;
    }else{
      return suits.indexOf(suitA) - suits.indexOf(suitB);
    }
}

const question1 = cards.reduce(function(acc, card) {  
    if(card[0] === "Spade"){
        acc ++;
    }
    return acc
}, 0)
console.log(question1)

// const question2 = cards.filter(function(card) {
//     if(card[1] === '2' || (card[1] === '3' && card[0] === ('Diamond' || card[0] === 'Heart'))){
//         return card
//     }
// })
// console.log(question2)

console.log('keep:')
console.log(cards.filter(card => compareCard(card, ['Club', '3']) >= 0))
console.log('remove:')
console.log(cards.filter(card => compareCard(card, ['Club', '3']) < 0))

// const question3 = cards.reduce(function(acc, card) {  
//     if((card[0] === "Diamond" || card[0] === "Heart") && 
//     (card[1] === "J" || card[1] === "Q" || card[1] === "K" || card[1] === "A")){
//         acc ++;
//     }
//     return acc
// }, 0)
// console.log(question3)

console.log(cards.map(function(card){
    if(card[0] === "Club"){
        return ['Diamond', card[1]]
    } else
    return card
})
)

console.log(cards.map(function(card){
    if(card[1] === "A"){
        return [card[0], "2"]
    } else
    return card
})
)