let unitLength = 20;
let boxColor = 0;
let strokeColor = 0;
let columns; /* To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;
let fr = document.querySelector('#speed')
let chooseColor = document.querySelector('#chooseColor')
let chooseBgColor = document.querySelector('#chooseBgColor')

function setup() {
    /* Set the canvas to be under the element #canvas*/
    let canvas = createCanvas(windowWidth, windowHeight - 100);
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    };
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init();  // Set the initial values of the currentBoard and nextBoard
}

/** Initialize/reset the board state */
function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

function draw() {
    background(chooseBgColor.value);

    generate();
    frameRate(parseInt(fr.value))
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] != 0) {
                fill(currentBoard[i][j]);
            } else {
                fill(chooseBgColor.value);
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            let peerColor;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    if (currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] != 0) {
                        peerColor = currentBoard[(x + i + columns) % columns][(y + j + rows) % rows]
                        neighbors += 1
                    }
                }
            }

            // Rules of Life
            if (currentBoard[x][y] != 0 && neighbors < 2) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] != 0 && neighbors > 3) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == 3) {
                // New life due to Reproduction
                nextBoard[x][y] = peerColor;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

function mouseDragged() {
    /* If the mouse coordinate is outside the board */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = chooseColor.value;
    fill(chooseColor.value);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/* When mouse is pressed */
function mousePressed() {
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    noLoop();
    mouseDragged();
}

//  * When mouse is released */
function mouseReleased() {
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    loop();
}


document.querySelector('#start')
    .addEventListener('click', function () {
        loop();
    });


document.querySelector('#stop')
    .addEventListener('click', function () {
        noLoop();
    });

document.querySelector('#reset-game')
    .addEventListener('click', function () {
        init();
        loop();
    });

document.querySelector('#random')
    .addEventListener('click', function () {
        for (let i = 0; i < columns; i++) {
            for (let j = 0; j < rows; j++) {
                currentBoard[i][j] = random() > 0.8 ? color(random(255), random(255), random(255)) : 0;
                nextBoard[i][j] = 0;
            }
        }
    });

function windowResized() {
    resizeCanvas(windowWidth - 100, windowHeight - 100);
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = [];
    }
    init();
}
