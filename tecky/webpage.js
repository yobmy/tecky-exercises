let bang = document.querySelector('#bang')
let nine = document.querySelector('#nine')
let dong = document.querySelector('#dong')
let miu = document.querySelector('#miu')
let chi = document.querySelector('#chi')
let main = document.querySelector('.main')
let homepage = document.querySelector('.homepage')
let arrow = document.querySelector('.arrow')

// image to redirect to homepage
bang.addEventListener('click', ()=>{
    main.classList.add('d-none')
    homepage.classList.remove('d-none')
    arrow.classList.remove('d-none')
    document.documentElement.className += 'bang-cursor'
})
nine.addEventListener('click', ()=>{
    main.classList.add('d-none')
    homepage.classList.remove('d-none')
    arrow.classList.remove('d-none')
    document.documentElement.className += 'nine-cursor'
})
dong.addEventListener('click', ()=>{
    main.classList.add('d-none')
    homepage.classList.remove('d-none')
    arrow.classList.remove('d-none')
    document.documentElement.className += 'dong-cursor'
})
miu.addEventListener('click', ()=>{
    main.classList.add('d-none')
    homepage.classList.remove('d-none')
    arrow.classList.remove('d-none')
    document.documentElement.className += 'miu-cursor'
})
chi.addEventListener('click', ()=>{
    main.classList.add('d-none')
    homepage.classList.remove('d-none')
    arrow.classList.remove('d-none')
    document.documentElement.className += 'chi-cursor'
})
