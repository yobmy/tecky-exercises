
const cells = document.querySelectorAll('.cell')
const circle = '<i class="far fa-circle icon white">'
const times = '<i class="fas fa-times icon grey">'
const player = document.querySelector('#turn')
const circleScore = document.querySelector('span.circle-win')
const timesScore = document.querySelector('span.times-win')
const button = document.querySelector('button')
const playerX = player.innerHTML
let turn = 1
let circleWin = [];
let timesWin = [];
let winning = [
    ["1", "2", "3"],
    ["1", "4", "7"],
    ["1", "5", "9"],
    ["2", "5", "8"],
    ["3", "6", "9"],
    ["3", "5", "7"],
    ["4", "5", "6"],
    ["7", "8", "9"],
]

for (let cell of cells) {
    cell.addEventListener('click', function (event) {
        let hasWon = checkWinning(circleWin) || checkWinning(timesWin)
        if (cell.innerHTML == "" && !hasWon) {
            if (turn % 2 === 1) {
                cell.innerHTML = times
                player.innerHTML = '<i class="far fa-circle icon-small"></i> Turn'
                timesWin.push(event.target.id);
                turn ++;
                if (checkWinning(timesWin)) {
                    player.innerHTML = 'Times Win'
                };
            } else {
                cell.innerHTML = circle
                player.innerHTML = playerX
                circleWin.push(event.target.id)
                turn ++;
                if (checkWinning(circleWin)) {
                    player.innerHTML = 'Circle Win'
                }
            }
        }

        console.log(timesWin, checkWinning(timesWin))
        console.log(circleWin, checkWinning(circleWin))
    })
}

function checkWinning(result) {
    let count = 0;
    for (let num of winning) {
        if (result.includes(num[0]) && result.includes(num[1]) && result.includes(num[2])) {
            return true
        }
    }
}




button.addEventListener('click', function (reset) {
    for (let cell of cells) {
        cell.innerHTML = "";
        player.innerHTML = playerX
        circleWin = [];
        timesWin = [];
    }
})






// if(checkWinning(winning, circleWin)){
//     console.log('circle wins')
// } else if (checkWinning(winning, timesWin)){
//     console.log('times wins')

// }